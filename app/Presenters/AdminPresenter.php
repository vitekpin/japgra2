<?php


namespace App\Presenters;
use Nette;
use Nette\ComponentModel\IComponent;

final class AdminPresenter extends \Nette\Application\UI\Presenter
{
    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    public function renderDefault(): void
    {
        $this->template->entries = $this->database->table('entries')
            ->order('id DESC')
            ->limit(5);
    }

    public function renderExamples(): void {

    }

    protected function createComponentEntryForm(): Nette\Application\UI\Form
    {
        $form = new Nette\Application\UI\Form();
        $form->addText('headWord','Klíčové slovo')->setRequired()->setHtmlAttribute('class', 'form-control');
        $form->addText('headWordJapanese', 'Japonsky')->setHtmlAttribute('class','form-control');
        $form->addText('type','Typ')->setHtmlAttribute('class', 'form-control');
        $form->addText('usageRestriction','Omezení použití')->setHtmlAttribute('class', 'form-control');
        $form->addTextArea('meaning', 'Význam')->setHtmlAttribute('class','form-control');
        $form->addText('counterpart', 'Protějšek')->setHtmlAttribute('class','form-control');
        $form->addText('relExp','Související výraz')->setHtmlAttribute('class','form-control');
        $form->addSubmit('save','Uložit')->setHtmlAttribute('class', 'btn btn-primary');
        $form->onSuccess[]=[$this,'entryFormSucceeded'];

        return $form;
    }

    public function entryFormSucceeded(Nette\Application\UI\Form $form, \stdClass $values): void {
        $this->database->table('entries')->insert([
            'headWord' => $values->headWord,
            'headWordJapanese' => $values->headWordJapanese,
            'type' => $values->type,
            'usageRestriction' => $values->usageRestriction,
            'meaning' => $values->meaning,
            'counterpart' => $values->counterpart,
            'relExp' => $values->relExp
        ]);

        $this->flashMessage('Uloženo', 'success');
        $this->redirect('this');

    }

    public function getPairs(){

        return $this->database->fetchPairs('SELECT id, headWord FROM entries order by headWord');
    }
    protected function createComponentExamplesForm(): Nette\Application\UI\Form {

        $form = new Nette\Application\UI\Form();
        $form->addSelect('entry_id','Entry',$this->getPairs())
            ->setHtmlAttribute('class','btn btn-success dropdown-toggle');
        $form->addText('example','Příklad')->setHtmlAttribute('class','form-control');
        $form->addText('translation','Překlad')->setHtmlAttribute('class','form-control');
        $form->addSubmit('save','Uložit')->setHtmlAttribute('class', 'btn btn-primary');
        $form->onSuccess[]=[$this,'examplesFormSucceeded'];

        return $form;
    }




}